/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 11:10:48 by qdam              #+#    #+#             */
/*   Updated: 2021/06/27 18:55:24 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

static void	init_phi(t_phi *phi, int id, t_state state, t_time_ms last_eat)
{
	phi->id = id;
	phi->state = state;
	phi->fork = 0;
	phi->last_eat = last_eat;
	phi->eaten = 0;
	phi->alive = true;
}

bool	my_sem_init(sem_t **sem_ptr, const char *name, unsigned int val)
{
	sem_t	*sem;

	sem = sem_open(name, O_CREAT, 0644, val);
	sem_unlink(name);
	sem_close(sem);
	sem = sem_open(name, O_CREAT | O_EXCL, 0644, val);
	if (sem == SEM_FAILED)
		return (false);
	*sem_ptr = sem;
	return (true);
}

bool	run(t_program *pr)
{
	t_phi	phi;
	int		i;
	pid_t	pid;
	pid_t	pool[1000];

	pr->start = get_current_time();
	i = -1;
	while (++i < pr->n_phi)
	{
		pid = fork();
		if (pid < 0)
			return (false);
		else if (!pid)
		{
			init_phi(&phi, i, THINK, pr->start);
			start_philo(pr, &phi);
		}
		else
			pool[i] = pid;
	}
	pool[i] = -9999;
	i = -1;
	while (pool[++i] != -9999)
		waitpid(pool[i], NULL, 0);
	return (true);
}
