/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/23 13:40:13 by qdam              #+#    #+#             */
/*   Updated: 2021/06/26 15:48:41 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

void	putnbr_to_buf(unsigned long num, char *buf_end)
{
	if (num >= 10)
		putnbr_to_buf(num / 10, buf_end - 1);
	*buf_end = '0' + num % 10;
}

void	print_change(t_program *pr, t_phi *phi, t_state_change change)
{
	char		buf[1024];
	t_time_ms	stamp;
	size_t		i;

	stamp = get_current_time() - pr->start;
	i = ft_numlen(stamp) - 1;
	putnbr_to_buf(stamp, &buf[i]);
	buf[++i] = ' ';
	i += ft_numlen(phi->id);
	putnbr_to_buf(phi->id, &buf[i]);
	buf[++i] = ' ';
	ft_strcpy(&buf[++i], g_changes[change]);
	while (buf[i])
		i++;
	write(1, buf, i);
}
