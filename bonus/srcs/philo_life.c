/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_life.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 15:35:55 by qdam              #+#    #+#             */
/*   Updated: 2021/06/29 01:16:45 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

static void	*start_police(void *prog)
{
	t_program	*pr;
	t_phi		*phi;

	pr = ((void **)prog)[0];
	phi = ((void **)prog)[1];
	while (phi->eaten != pr->n_eat)
	{
		if (get_current_time() - phi->last_eat > pr->die_ms)
		{
			phi->alive = false;
			print_change(pr, phi, DIED);
			destroy_prog(pr);
			kill(0, SIGINT);
		}
	}
	return (NULL);
}

static inline
void	phi_eat(t_program *pr, t_phi *phi)
{
	phi->last_eat = get_current_time();
	print_change(pr, phi, EATING);
	ms_sleep(pr->eat_ms);
	sem_post(pr->forks);
	sem_post(pr->forks);
	phi->eaten++;
}

static inline
void	phi_sleep(t_program *pr, t_phi *phi)
{
	print_change(pr, phi, SLEEPING);
	ms_sleep(pr->slp_ms);
}

static inline
void	phi_think(t_program *pr, t_phi *phi)
{
	print_change(pr, phi, THINKING);
	sem_wait(pr->forks);
	print_change(pr, phi, TAKE_FORK);
	sem_wait(pr->forks);
	print_change(pr, phi, TAKE_FORK);
}

void	start_philo(t_program *pr, t_phi *phi)
{
	const void	*ptr[] = {pr, phi};
	pthread_t	police;

	pthread_create(&police, NULL, start_police, ptr);
	while (phi->alive && phi->eaten != pr->n_eat)
	{
		if (phi->state == EAT)
			phi_eat(pr, phi);
		else if (phi->state == SLEEP)
			phi_sleep(pr, phi);
		else
			phi_think(pr, phi);
		phi->state = (phi->state + 1) % 3;
	}
	if (phi->eaten == pr->n_eat)
		print_change(pr, phi, ENOUGH);
	pthread_detach(police);
	exit(0);
}
