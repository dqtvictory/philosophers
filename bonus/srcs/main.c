/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 11:06:12 by qdam              #+#    #+#             */
/*   Updated: 2021/06/26 14:38:14 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

static int	args_error(void)
{
	int			i;
	static char	*lines[] = {
		"The program must be executed as following:",
		"./philo num_philos time_die time_eat time_sleep [num_eat], in which:",
		"- num_philos: Number of philosophers (at least 1)",
		"- time_die: time one dies if he doesn't eat",
		"- time_eat: time one takes to eat",
		"- time_sleep: time one takes to sleep",
		"- (optional) num_eat: maximum number of times one eats before ending",
		NULL
	};

	i = -1;
	while (lines[++i])
	{
		if (i == 0)
			printf("\n\033[91m");
		else if (i == 1)
			printf("\033[93m");
		else
			printf("\033[1m");
		printf("%s\033[0m\n", lines[i]);
	}
	return (printf("\n"));
}

static bool	check_args(int ac, char **av, t_program *pr)
{
	int		i;
	size_t	len;

	i = -1;
	while (++i < ac)
	{
		len = ft_strlen(av[i]);
		if (!len || len > 10 || !ft_isnum(av[i]))
			return (false);
		if (!ft_atoi_philo(av[i]))
			return (false);
	}
	pr->n_phi = ft_atoi_philo(av[0]);
	pr->die_ms = (t_time_ms)ft_atoi_philo(av[1]);
	pr->eat_ms = (t_time_ms)ft_atoi_philo(av[2]);
	pr->slp_ms = (t_time_ms)ft_atoi_philo(av[3]);
	pr->n_eat = -1;
	if (ac == 5)
		pr->n_eat = ft_atoi_philo(av[4]);
	return (true);
}

static int	one_philo_die(t_program *pr)
{
	printf("0 1 %s", g_changes[THINKING]);
	printf("0 1 %s", g_changes[TAKE_FORK]);
	ms_sleep(pr->die_ms);
	printf("%lu 1 %s", pr->die_ms + 1, g_changes[DIED]);
	return (0);
}

int	main(int ac, char **av)
{
	char		*error;
	t_program	pr;

	if (ac != 5 && ac != 6)
		return (args_error());
	memset(&pr, 0, sizeof(t_program));
	if (!check_args(ac - 1, av + 1, &pr))
		return (args_error());
	if (pr.n_phi == 1)
		return (one_philo_die(&pr));
	error = NULL;
	if (!my_sem_init(&pr.forks, FORKS_SEM, pr.n_phi))
		error = ERROR_FORKS;
	else if (!my_sem_init(&pr.glock, GLOCK_SEM, 1))
		error = ERROR_GLOCK;
	else if (!run(&pr))
		error = ERROR_PROCS;
	destroy_prog(&pr);
	if (error)
		return (printf("%s\n", error) != 0);
}
