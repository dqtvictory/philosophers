/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proc_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/26 14:41:50 by qdam              #+#    #+#             */
/*   Updated: 2021/06/29 01:22:57 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"

t_time_ms	get_current_time(void)
{
	struct timeval	now;

	gettimeofday(&now, NULL);
	return ((now.tv_sec * 1000) + (now.tv_usec / 1000));
}

void	ms_sleep(t_time_ms ms)
{
	t_time_ms	start;
	t_time_ms	now;

	start = get_current_time();
	now = start;
	while (ms >= get_current_time() - start)
		usleep(100);
}

void	destroy_prog(t_program *pr)
{
	if (pr->forks)
	{
		sem_unlink(FORKS_SEM);
		sem_close(pr->forks);
	}
	if (pr->glock)
	{
		sem_unlink(GLOCK_SEM);
		sem_close(pr->glock);
	}
	pr->forks = NULL;
	pr->glock = NULL;
}
