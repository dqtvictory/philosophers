/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_bonus.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/14 19:11:09 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/27 19:04:48 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_BONUS_H
# define PHILO_BONUS_H

# include <fcntl.h>
# include <pthread.h>
# include <signal.h>
# include <string.h>
# include <stdbool.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/wait.h>
# include <unistd.h>

# include "literals.h"
# include "structs_enums.h"

size_t		ft_strlen(char *str);
size_t		ft_numlen(unsigned long num);
void		ft_strcpy(char *des, char *src);
bool		ft_isnum(char *str);
int			ft_atoi_philo(char *str);

bool		my_sem_init(sem_t **sem_ptr, const char *name, unsigned int val);
bool		init_philos(t_program *pr);
bool		run(t_program *pr);

void		destroy_prog(t_program *pr);
void		ms_sleep(t_time_ms ms);
t_time_ms	get_current_time(void);

void		print_change(t_program *pr, t_phi *phi, t_state_change change);
void		start_philo(t_program *pr, t_phi *phi);
#endif
