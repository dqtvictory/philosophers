/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_enums.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:15:42 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/26 16:32:28 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_ENUMS_H
# define STRUCTS_ENUMS_H

# include <semaphore.h>
# include <sys/time.h>

typedef unsigned long	t_time_ms;

typedef enum e_state
{
	EAT,
	SLEEP,
	THINK,
}	t_state;

typedef enum e_state_change
{
	TAKE_FORK,
	EATING,
	SLEEPING,
	THINKING,
	DIED,
	ENOUGH,
}	t_state_change;

typedef struct s_philosopher
{
	int			id;
	t_state		state;
	int			fork;
	t_time_ms	last_eat;
	int			eaten;
	bool		alive;
}	t_phi;

typedef struct s_program
{
	int			n_phi;
	t_time_ms	die_ms;
	t_time_ms	eat_ms;
	t_time_ms	slp_ms;
	int			n_eat;
	t_time_ms	start;
	sem_t		*forks;
	sem_t		*glock;
}	t_program;

#endif
