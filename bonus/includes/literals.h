/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   literals.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:10:38 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/26 16:20:11 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LITERALS_H
# define LITERALS_H

static char	*g_changes[] = {
	"has taken a fork\n",
	"is eating\n",
	"is sleeping\n",
	"is thinking\n",
	"died\n",
	"has eaten enough\n"
};

# define FORKS_SEM		"sem_forks"
# define GLOCK_SEM		"sem_glock"

# define ERROR_FORKS	"Error while creating forks' semaphore"
# define ERROR_GLOCK	"Error while creating global semaphore"
# define ERROR_PROCS	"Error while creating process"

#endif
