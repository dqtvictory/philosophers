/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_life.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/16 11:41:51 by qdam              #+#    #+#             */
/*   Updated: 2021/06/26 10:31:24 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static inline
void	phi_eat(t_program *pr, t_phi *phi)
{
	phi->last_eat = get_current_time();
	print_change(pr, phi, EATING);
	ms_sleep(pr->eat_ms);
	pthread_mutex_unlock(phi->fork[0]);
	pthread_mutex_unlock(phi->fork[1]);
	phi->eaten++;
}

static inline
void	phi_sleep(t_program *pr, t_phi *phi)
{
	print_change(pr, phi, SLEEPING);
	ms_sleep(pr->slp_ms);
}

static inline
void	phi_think(t_program *pr, t_phi *phi)
{
	print_change(pr, phi, THINKING);
	if (!pr->dead)
	{
		pthread_mutex_lock(phi->fork[0]);
		print_change(pr, phi, TAKE_FORK);
	}
	if (pr->dead)
	{
		pthread_mutex_unlock(phi->fork[0]);
		return ;
	}
	if (!pr->dead)
	{
		pthread_mutex_lock(phi->fork[1]);
		print_change(pr, phi, TAKE_FORK);
	}
	if (pr->dead)
	{
		pthread_mutex_unlock(phi->fork[1]);
		return ;
	}
}

static inline
void	phi_next_action(t_program *pr, t_phi *phi)
{
	if (phi->state == EAT)
		phi_eat(pr, phi);
	else if (phi->state == SLEEP)
		phi_sleep(pr, phi);
	else
		phi_think(pr, phi);
	phi->state = (phi->state + 1) % 3;
}

void	*start_philo(void *prog)
{
	t_phi		*phi;
	t_program	*pr;

	pr = (t_program *)prog;
	pthread_mutex_lock(&pr->glock);
	phi = &pr->philos[pr->tmp++];
	pthread_mutex_unlock(&pr->glock);
	while (!pr->dead && phi->eaten != pr->n_eat)
		phi_next_action(pr, phi);
	if (phi->eaten == pr->n_eat)
		print_change(pr, phi, ENOUGH);
	return (NULL);
}
