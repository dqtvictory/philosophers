/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:33:56 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/30 14:46:19 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_time_ms	get_current_time(void)
{
	struct timeval	now;

	gettimeofday(&now, NULL);
	return ((now.tv_sec * 1000) + (now.tv_usec / 1000));
}

void	ms_sleep(t_time_ms ms)
{
	t_time_ms	start;
	t_time_ms	now;

	start = get_current_time();
	now = start;
	while (ms > get_current_time() - start)
		usleep(100);
}

void	destroy_prog(t_program *pr)
{
	int	i;

	if (pr->forks)
	{
		i = -1;
		while (++i < pr->n_phi)
			pthread_mutex_destroy(&(pr->forks[i]));
		free(pr->forks);
	}
	if (pr->philos)
		free(pr->philos);
	pr->forks = NULL;
	pr->philos = NULL;
}

void	*start_police(void *prog)
{
	t_program	*pr;
	int			i;

	pr = (t_program *)prog;
	i = -1;
	while (pr->philos[++i].eaten != pr->n_eat)
	{
		if (get_current_time() - pr->philos[i].last_eat > pr->die_ms)
		{
			pr->dead = true;
			print_change(pr, &pr->philos[i], DIED);
			break ;
		}
		i = i * (i != pr->n_phi - 1) - (i == pr->n_phi - 1);
	}
	i = -1;
	while (++i < pr->n_phi)
		pthread_mutex_destroy(&(pr->forks[i]));
	return (NULL);
}
