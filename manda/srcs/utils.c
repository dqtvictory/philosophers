/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/14 21:25:06 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/23 15:37:11 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

size_t	ft_strlen(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

size_t	ft_numlen(unsigned long num)
{
	size_t	len;

	if (!num)
		return (1);
	len = 0;
	while (num)
	{
		num = num / 10;
		len++;
	}
	return (len);
}

void	ft_strcpy(char *des, char *src)
{
	size_t	i;

	i = 0;
	while (src[i])
	{
		des[i] = src[i];
		i++;
	}
	des[i] = 0;
}

bool	ft_isnum(char *str)
{
	while (*str)
	{
		if (*str < '0' || *str > '9')
			return (false);
		str++;
	}
	return (true);
}

int	ft_atoi_philo(char *str)
{
	long	num;
	long	sign;

	num = 0;
	sign = 1;
	if (*str == '-')
	{
		sign = -1;
		str++;
	}
	while (*str)
	{
		num = num * 10 + (*str - '0');
		str++;
	}
	if (num < -2147483648 || num > 2147483647)
		return (0);
	return ((int)(num * sign));
}
