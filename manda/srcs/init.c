/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/15 14:41:10 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/26 10:23:48 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

bool	init_forks(t_program *pr)
{
	int	i;

	pr->forks = malloc(sizeof(pthread_mutex_t) * pr->n_phi);
	if (!pr->forks)
		return (false);
	i = -1;
	while (++i < pr->n_phi)
		if (pthread_mutex_init(&pr->forks[i], NULL))
			return (false);
	return (true);
}

bool	init_philos(t_program *pr)
{
	int		i;
	t_phi	*phi;

	pr->philos = malloc(sizeof(t_phi) * pr->n_phi);
	if (!pr->philos)
		return (false);
	i = -1;
	while (++i < pr->n_phi)
	{
		phi = &pr->philos[i];
		phi->id = i + 1;
		phi->state = (i % 2) * SLEEP + (!(i % 2)) * THINK;
		phi->fork[0] = &pr->forks[i];
		phi->fork[1] = &pr->forks[(i + 1) % pr->n_phi];
		phi->eaten = 0;
	}
	return (true);
}

bool	run(t_program *pr)
{
	pthread_t	police;
	int			i;

	pr->start = get_current_time();
	pr->tmp = 0;
	i = -1;
	while (++i < pr->n_phi)
	{
		pr->philos[i].last_eat = pr->start;
		if (pthread_create(&pr->philos[i].thread, NULL, start_philo, pr))
			return (false);
	}
	ms_sleep(1);
	if (pthread_create(&police, NULL, start_police, pr))
		return (false);
	i = -1;
	pthread_join(police, NULL);
	while (++i < pr->n_phi)
		pthread_join(pr->philos[i].thread, NULL);
	return (true);
}
