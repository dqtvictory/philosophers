/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_enums.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:15:42 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/23 16:31:02 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_ENUMS_H
# define STRUCTS_ENUMS_H

# include <pthread.h>
# include <sys/time.h>

typedef unsigned long	t_time_ms;

typedef enum e_state
{
	EAT,
	SLEEP,
	THINK,
}	t_state;

typedef enum e_state_change
{
	TAKE_FORK,
	EATING,
	SLEEPING,
	THINKING,
	DIED,
	ENOUGH,
}	t_state_change;

typedef struct s_philosopher
{
	int				id;
	pthread_t		thread;
	t_state			state;
	pthread_mutex_t	*fork[2];
	t_time_ms		last_eat;
	int				eaten;
}	t_phi;

typedef struct s_program
{
	int				n_phi;
	t_time_ms		die_ms;
	t_time_ms		eat_ms;
	t_time_ms		slp_ms;
	int				n_eat;
	t_phi			*philos;
	pthread_mutex_t	*forks;
	bool			dead;
	t_time_ms		start;
	pthread_mutex_t	glock;
	int				tmp;
}	t_program;

#endif
