/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   literals.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:10:38 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/23 16:03:35 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LITERALS_H
# define LITERALS_H

static char	*g_changes[] = {
	"has taken a fork\n",
	"is eating\n",
	"is sleeping\n",
	"is thinking\n",
	"died\n",
	"has eaten enough\n"
};

# define ERROR_MUTEX	"Error while creating mutexes"
# define ERROR_GLOCK	"Error while creating global mutex"
# define ERROR_PHILO	"Error while creating philosophers"
# define ERROR_THREAD	"Error while creating threads"

#endif
