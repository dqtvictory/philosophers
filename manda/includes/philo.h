/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/14 19:11:09 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/25 12:15:33 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <string.h>
# include <stdio.h>
# include <stdbool.h>
# include <stdlib.h>
# include <unistd.h>

# include "literals.h"
# include "structs_enums.h"

size_t		ft_strlen(char *str);
size_t		ft_numlen(unsigned long num);
void		ft_strcpy(char *des, char *src);
bool		ft_isnum(char *str);
int			ft_atoi_philo(char *str);

bool		init_forks(t_program *pr);
bool		init_philos(t_program *pr);
bool		run(t_program *pr);

void		destroy_prog(t_program *pr);
void		ms_sleep(t_time_ms ms);
t_time_ms	get_current_time(void);

void		print_change(t_program *pr, t_phi *phi, t_state_change change);

void		*start_police(void *prog);
void		*start_philo(void *prog);

#endif
